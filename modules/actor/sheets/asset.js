// -*- js -*-
export default class ActorSheetDuneAsset extends ActorSheet {

  /** @override */
  get template() {
    return `systems/dune/templates/actors/asset-sheet.html`;
  }

  /** @override */
  getData() {
    const data = super.getData();
    console.log("ASSET", data);
    data.playerToken = this.actor.token && !game.user.isGM;
    return data;
  }

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      height: 300
    });
  }

}
