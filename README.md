# Dune: The Roleplaying Game (2d20) for FoundryVTT

Community contributed and maintained system for playing Dune 2D20 system with FoundryVTT (https://foundryvtt.com/).

Unofficial but approved by Modiphius

## Licence

- Content: [Dune Roleplaying Game](https://www.modiphius.net/pages/discover-dune-roleplaying-game) © Modiphius Entertainment.
- Foundry VTT: [Limited License Agreement for module development](https://foundryvtt.com/article/license/).
- Source code: Copyright © 2021 Bastien Durel, licenced under the [GPL 3.0](https://git.geekwu.org/dune/foundryvtt-dune-system/-/blob/master/LICENSE).

All copyright assets used with explicit consent from Modiphius Entertainment. The fvtt-modiphius developer community holds no claim to underlying copyrighted assets.

The 2d20 system and Modiphius Logos are copyright Modiphius Entertainment Ltd. 2015–2021.

All 2d20 system text is copyright Modiphius Entertainment Ltd.
